CREATE TABLE `calendar` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `endday` int NOT NULL,
    `name` varchar(255) NOT NULL,
    `note` longtext,
    `startday` int NOT NULL,
    CONSTRAINT `PK_calendar` PRIMARY KEY (`id`)
);
CREATE TABLE `effect_category` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `dicecount` smallint NOT NULL,
    `dicesize` smallint NOT NULL,
    `name` varchar(255) NOT NULL,
    `note` longtext NOT NULL,
    CONSTRAINT `PK_effect_category` PRIMARY KEY (`id`)
);
CREATE TABLE `error_report` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `data` json NOT NULL,
    `date` timestamp DEFAULT CURRENT_TIMESTAMP,
    `useragent` longtext,
    CONSTRAINT `PK_error_report` PRIMARY KEY (`id`)
);
CREATE TABLE `icon` (
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_icon` PRIMARY KEY (`name`)
);
CREATE TABLE `item_slot` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `count` smallint NOT NULL,
    `name` varchar(255) NOT NULL,
    `stackable` bit,
    `techname` varchar(255) NOT NULL,
    CONSTRAINT `PK_item_slot` PRIMARY KEY (`id`)
);
CREATE TABLE `item_type` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `name` varchar(255) NOT NULL,
    `note` longtext NOT NULL,
    `special` varchar(2048) NOT NULL,
    CONSTRAINT `PK_item_type` PRIMARY KEY (`id`)
);
CREATE TABLE `job` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `baseat` smallint,
    `baseea` smallint,
    `baseev` smallint,
    `baseprd` smallint,
    `bonusev` smallint,
    `diceealevelup` smallint,
    `factorev` double,
    `informations` longtext,
    `internalname` varchar(255),
    `ismagic` bit DEFAULT false,
    `maxarmorpr` smallint,
    `maxload` bigint,
    `name` varchar(255),
    `parentjob` bigint,
    `special` varchar(2048),
    CONSTRAINT `PK_job` PRIMARY KEY (`id`),
    CONSTRAINT `FK_job_job_parentjob` FOREIGN KEY (`parentjob`) REFERENCES `job` (`id`) ON DELETE NO ACTION
);
CREATE TABLE `location` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `data` json,
    `name` varchar(255) NOT NULL,
    `parent` bigint,
    CONSTRAINT `PK_location` PRIMARY KEY (`id`),
    CONSTRAINT `FK_location_location_parent` FOREIGN KEY (`parent`) REFERENCES `location` (`id`) ON DELETE NO ACTION
);
CREATE TABLE `monster_trait` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `description` longtext NOT NULL,
    `levels` json,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_monster_trait` PRIMARY KEY (`id`)
);
CREATE TABLE `monster_type` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `difficulty` varchar(255) NOT NULL,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_monster_type` PRIMARY KEY (`id`)
);
CREATE TABLE `origin` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `advantage` longtext,
    `baseea` smallint,
    `baseev` smallint,
    `bonusat` smallint,
    `bonusprd` smallint,
    `description` longtext NOT NULL,
    `diceevlevelup` int NOT NULL,
    `maxarmorpr` smallint,
    `maxload` bigint,
    `name` varchar(255) NOT NULL,
    `size` longtext,
    `special` varchar(2048),
    `speedmodifier` smallint,
    CONSTRAINT `PK_origin` PRIMARY KEY (`id`)
);
CREATE TABLE `quest_template` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `data` longtext NOT NULL,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_quest_template` PRIMARY KEY (`id`)
);
CREATE TABLE `skill` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `description` longtext,
    `name` varchar(255) NOT NULL,
    `require` longtext,
    `resist` longtext,
    `roleplay` longtext,
    `stat` varchar(255),
    `test` smallint,
    `using` longtext,
    CONSTRAINT `PK_skill` PRIMARY KEY (`id`)
);
CREATE TABLE `spell_category` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `default` bit NOT NULL,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_spell_category` PRIMARY KEY (`id`)
);
CREATE TABLE `stat` (
    `name` varchar(255) NOT NULL,
    `bonus` longtext,
    `description` longtext NOT NULL,
    `displayname` varchar(255) NOT NULL,
    `penality` longtext,
    CONSTRAINT `PK_stat` PRIMARY KEY (`name`)
);
CREATE TABLE `user` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `admin` bit NOT NULL DEFAULT false,
    `displayname` varchar(255),
    `fbid` varchar(255),
    `password` varchar(255),
    `username` varchar(255),
    CONSTRAINT `PK_user` PRIMARY KEY (`id`)
);
CREATE TABLE `effect` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `category` bigint NOT NULL,
    `combatcount` int,
    `description` longtext,
    `dice` smallint,
    `duration` longtext,
    `name` varchar(255) NOT NULL,
    `timeduration` int,
    CONSTRAINT `PK_effect` PRIMARY KEY (`id`),
    CONSTRAINT `FK_effect_effect_category_category` FOREIGN KEY (`category`) REFERENCES `effect_category` (`id`) ON DELETE CASCADE
);
CREATE TABLE `item_category` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `description` varchar(255) NOT NULL,
    `name` varchar(255) NOT NULL,
    `note` longtext NOT NULL,
    `techname` varchar(255) NOT NULL DEFAULT '',
    `type` bigint NOT NULL,
    CONSTRAINT `PK_item_category` PRIMARY KEY (`id`),
    CONSTRAINT `FK_item_category_item_type_type` FOREIGN KEY (`type`) REFERENCES `item_type` (`id`) ON DELETE CASCADE
);
CREATE TABLE `job_bonus` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `description` longtext NOT NULL,
    `jobid` bigint NOT NULL,
    `token` varchar(255) NOT NULL,
    CONSTRAINT `PK_job_bonus` PRIMARY KEY (`id`),
    CONSTRAINT `FK_job_bonus_job_jobid` FOREIGN KEY (`jobid`) REFERENCES `job` (`id`) ON DELETE CASCADE
);
CREATE TABLE `job_restrict` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `jobid` bigint NOT NULL,
    `text` longtext NOT NULL,
    CONSTRAINT `PK_job_restrict` PRIMARY KEY (`id`),
    CONSTRAINT `FK_job_restrict_job_jobid` FOREIGN KEY (`jobid`) REFERENCES `job` (`id`) ON DELETE CASCADE
);
CREATE TABLE `speciality` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `description` longtext NOT NULL,
    `job` bigint NOT NULL,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_speciality` PRIMARY KEY (`id`),
    CONSTRAINT `FK_speciality_job_job` FOREIGN KEY (`job`) REFERENCES `job` (`id`) ON DELETE CASCADE
);
CREATE TABLE `location_map` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `data` json NOT NULL,
    `file` varchar(255) NOT NULL,
    `gm` bit NOT NULL,
    `location` bigint NOT NULL,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_location_map` PRIMARY KEY (`id`),
    CONSTRAINT `FK_location_map_location_location` FOREIGN KEY (`location`) REFERENCES `location` (`id`) ON DELETE CASCADE
);
CREATE TABLE `monster_template` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `data` json NOT NULL,
    `name` varchar(255) NOT NULL,
    `type` bigint NOT NULL,
    CONSTRAINT `PK_monster_template` PRIMARY KEY (`id`),
    CONSTRAINT `FK_monster_template_monster_type_type` FOREIGN KEY (`type`) REFERENCES `monster_type` (`id`) ON DELETE CASCADE
);
CREATE TABLE `job_origin_blacklist` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `job` bigint NOT NULL,
    `origin` bigint NOT NULL,
    CONSTRAINT `PK_job_origin_blacklist` PRIMARY KEY (`id`),
    CONSTRAINT `FK_job_origin_blacklist_job_job` FOREIGN KEY (`job`) REFERENCES `job` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_job_origin_blacklist_origin_origin` FOREIGN KEY (`origin`) REFERENCES `origin` (`id`) ON DELETE CASCADE
);
CREATE TABLE `job_origin_whitelist` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `job` bigint NOT NULL,
    `origin` bigint NOT NULL,
    CONSTRAINT `PK_job_origin_whitelist` PRIMARY KEY (`id`),
    CONSTRAINT `FK_job_origin_whitelist_job_job` FOREIGN KEY (`job`) REFERENCES `job` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_job_origin_whitelist_origin_origin` FOREIGN KEY (`origin`) REFERENCES `origin` (`id`) ON DELETE CASCADE
);
CREATE TABLE `origin_bonus` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `description` longtext NOT NULL,
    `originid` bigint NOT NULL,
    `token` varchar(255) NOT NULL,
    CONSTRAINT `PK_origin_bonus` PRIMARY KEY (`id`),
    CONSTRAINT `FK_origin_bonus_origin_originid` FOREIGN KEY (`originid`) REFERENCES `origin` (`id`) ON DELETE CASCADE
);
CREATE TABLE `origin_info` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `description` longtext NOT NULL,
    `originid` bigint NOT NULL,
    `title` varchar(255) NOT NULL,
    CONSTRAINT `PK_origin_info` PRIMARY KEY (`id`),
    CONSTRAINT `FK_origin_info_origin_originid` FOREIGN KEY (`originid`) REFERENCES `origin` (`id`) ON DELETE CASCADE
);
CREATE TABLE `origin_restrict` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `originid` bigint NOT NULL,
    `text` longtext NOT NULL,
    `tokens` varchar(255) NOT NULL,
    CONSTRAINT `PK_origin_restrict` PRIMARY KEY (`id`),
    CONSTRAINT `FK_origin_restrict_origin_originid` FOREIGN KEY (`originid`) REFERENCES `origin` (`id`) ON DELETE CASCADE
);
CREATE TABLE `job_skill` (
    `jobid` bigint NOT NULL,
    `skillid` bigint NOT NULL,
    `default` bit NOT NULL,
    CONSTRAINT `PK_job_skill` PRIMARY KEY (`jobid`, `skillid`),
    CONSTRAINT `FK_job_skill_job_jobid` FOREIGN KEY (`jobid`) REFERENCES `job` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_job_skill_skill_skillid` FOREIGN KEY (`skillid`) REFERENCES `skill` (`id`) ON DELETE CASCADE
);
CREATE TABLE `origin_skill` (
    `originid` bigint NOT NULL,
    `skillid` bigint NOT NULL,
    `default` bit NOT NULL,
    CONSTRAINT `PK_origin_skill` PRIMARY KEY (`originid`, `skillid`),
    CONSTRAINT `FK_origin_skill_origin_originid` FOREIGN KEY (`originid`) REFERENCES `origin` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_origin_skill_skill_skillid` FOREIGN KEY (`skillid`) REFERENCES `skill` (`id`) ON DELETE CASCADE
);
CREATE TABLE `skill_effect` (
    `skill` bigint NOT NULL,
    `stat` varchar(255) NOT NULL,
    `value` bigint NOT NULL,
    CONSTRAINT `PK_skill_effect` PRIMARY KEY (`skill`, `stat`),
    CONSTRAINT `FK_skill_effect_skill_skill` FOREIGN KEY (`skill`) REFERENCES `skill` (`id`) ON DELETE CASCADE
);
CREATE TABLE `job_requirement` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `jobid` bigint NOT NULL,
    `maxvalue` bigint,
    `minvalue` bigint,
    `stat` varchar(255) NOT NULL,
    CONSTRAINT `PK_job_requirement` PRIMARY KEY (`id`),
    CONSTRAINT `FK_job_requirement_job_jobid` FOREIGN KEY (`jobid`) REFERENCES `job` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_job_requirement_stat_stat` FOREIGN KEY (`stat`) REFERENCES `stat` (`name`) ON DELETE CASCADE
);
CREATE TABLE `origin_requirement` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `maxvalue` bigint,
    `minvalue` bigint,
    `originid` bigint NOT NULL,
    `stat` varchar(255) NOT NULL,
    CONSTRAINT `PK_origin_requirement` PRIMARY KEY (`id`),
    CONSTRAINT `FK_origin_requirement_origin_originid` FOREIGN KEY (`originid`) REFERENCES `origin` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_origin_requirement_stat_stat` FOREIGN KEY (`stat`) REFERENCES `stat` (`name`) ON DELETE CASCADE
);
CREATE TABLE `spell` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `category` bigint NOT NULL,
    `cost` bigint NOT NULL,
    `description` longtext NOT NULL,
    `distance` bigint,
    `distancenote` longtext,
    `durationeffect` bigint NOT NULL,
    `durationeffectunit` varchar(255) NOT NULL,
    `durationprepare` bigint NOT NULL,
    `durationprepareunit` varchar(255) NOT NULL,
    `name` varchar(255) NOT NULL,
    `speed` bigint NOT NULL,
    `test` bigint NOT NULL,
    `teststat` varchar(255) NOT NULL,
    `type` varchar(255) NOT NULL,
    `variablecost` bit NOT NULL,
    CONSTRAINT `PK_spell` PRIMARY KEY (`id`),
    CONSTRAINT `FK_spell_spell_category_category` FOREIGN KEY (`category`) REFERENCES `spell_category` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_spell_stat_teststat` FOREIGN KEY (`teststat`) REFERENCES `stat` (`name`) ON DELETE CASCADE
);
CREATE TABLE `user_session` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `expire` timestamp NOT NULL,
    `ip` varchar(255),
    `key` varchar(255) NOT NULL,
    `start` timestamp DEFAULT CURRENT_TIMESTAMP,
    `userid` bigint NOT NULL,
    CONSTRAINT `PK_user_session` PRIMARY KEY (`id`),
    CONSTRAINT `FK_user_session_user_userid` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE
);
CREATE TABLE `effect_modifier` (
    `effect` bigint NOT NULL,
    `stat` varchar(255) NOT NULL,
    `type` varchar(255) NOT NULL DEFAULT 'ADD',
    `value` bigint NOT NULL,
    CONSTRAINT `PK_effect_modifier` PRIMARY KEY (`effect`, `stat`),
    CONSTRAINT `FK_effect_modifier_effect_effect` FOREIGN KEY (`effect`) REFERENCES `effect` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_effect_modifier_stat_stat` FOREIGN KEY (`stat`) REFERENCES `stat` (`name`) ON DELETE CASCADE
);
CREATE TABLE `item_template` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `category` bigint NOT NULL,
    `cleanname` varchar(255) DEFAULT NULL,
    `data` json,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_item_template` PRIMARY KEY (`id`),
    CONSTRAINT `FK_item_template_item_category_category` FOREIGN KEY (`category`) REFERENCES `item_category` (`id`) ON DELETE CASCADE
);
CREATE TABLE `speciality_modifier` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `speciality` bigint NOT NULL,
    `stat` varchar(255) NOT NULL,
    `value` bigint NOT NULL,
    CONSTRAINT `PK_speciality_modifier` PRIMARY KEY (`id`),
    CONSTRAINT `FK_speciality_modifier_speciality_speciality` FOREIGN KEY (`speciality`) REFERENCES `speciality` (`id`) ON DELETE CASCADE
);
CREATE TABLE `speciality_special` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `description` longtext NOT NULL,
    `isbonus` bit NOT NULL,
    `speciality` bigint NOT NULL,
    `token` varchar(255),
    CONSTRAINT `PK_speciality_special` PRIMARY KEY (`id`),
    CONSTRAINT `FK_speciality_special_speciality_speciality` FOREIGN KEY (`speciality`) REFERENCES `speciality` (`id`) ON DELETE CASCADE
);
CREATE TABLE `monster_location` (
    `monsterid` bigint NOT NULL,
    `locationid` bigint NOT NULL,
    CONSTRAINT `PK_monster_location` PRIMARY KEY (`monsterid`, `locationid`),
    CONSTRAINT `FK_monster_location_location_locationid` FOREIGN KEY (`locationid`) REFERENCES `location` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_monster_location_monster_template_monsterid` FOREIGN KEY (`monsterid`) REFERENCES `monster_template` (`id`) ON DELETE CASCADE
);
CREATE TABLE `item_effect` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `item` bigint NOT NULL,
    `requirejob` bigint,
    `requireorigin` bigint,
    `special` varchar(2048),
    `stat` varchar(255) NOT NULL,
    `type` varchar(255) NOT NULL DEFAULT 'ADD',
    `value` bigint NOT NULL,
    CONSTRAINT `PK_item_effect` PRIMARY KEY (`id`),
    CONSTRAINT `FK_item_effect_item_template_item` FOREIGN KEY (`item`) REFERENCES `item_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_item_effect_job_requirejob` FOREIGN KEY (`requirejob`) REFERENCES `job` (`id`) ON DELETE NO ACTION,
    CONSTRAINT `FK_item_effect_origin_requireorigin` FOREIGN KEY (`requireorigin`) REFERENCES `origin` (`id`) ON DELETE NO ACTION,
    CONSTRAINT `FK_item_effect_stat_stat` FOREIGN KEY (`stat`) REFERENCES `stat` (`name`) ON DELETE CASCADE
);
CREATE TABLE `item_requirement` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `item` bigint NOT NULL,
    `maxvalue` bigint,
    `minvalue` bigint,
    `stat` varchar(255) NOT NULL,
    CONSTRAINT `PK_item_requirement` PRIMARY KEY (`id`),
    CONSTRAINT `FK_item_requirement_item_template_item` FOREIGN KEY (`item`) REFERENCES `item_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_item_requirement_stat_stat` FOREIGN KEY (`stat`) REFERENCES `stat` (`name`) ON DELETE CASCADE
);
CREATE TABLE `item_skill` (
    `skill` bigint NOT NULL,
    `item` bigint NOT NULL,
    CONSTRAINT `PK_item_skill` PRIMARY KEY (`skill`, `item`),
    CONSTRAINT `FK_item_skill_item_template_item` FOREIGN KEY (`item`) REFERENCES `item_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_item_skill_skill_skill` FOREIGN KEY (`skill`) REFERENCES `skill` (`id`) ON DELETE CASCADE
);
CREATE TABLE `item_skill_modifiers` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `item` bigint NOT NULL,
    `skill` bigint NOT NULL,
    `value` smallint NOT NULL,
    CONSTRAINT `PK_item_skill_modifiers` PRIMARY KEY (`id`),
    CONSTRAINT `FK_item_skill_modifiers_item_template_item` FOREIGN KEY (`item`) REFERENCES `item_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_item_skill_modifiers_skill_skill` FOREIGN KEY (`skill`) REFERENCES `skill` (`id`) ON DELETE CASCADE
);
CREATE TABLE `item_template_slot` (
    `slot` bigint NOT NULL,
    `item` bigint NOT NULL,
    CONSTRAINT `PK_item_template_slot` PRIMARY KEY (`slot`, `item`),
    CONSTRAINT `FK_item_template_slot_item_template_item` FOREIGN KEY (`item`) REFERENCES `item_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_item_template_slot_item_slot_slot` FOREIGN KEY (`slot`) REFERENCES `item_slot` (`id`) ON DELETE CASCADE
);
CREATE TABLE `item_unskill` (
    `skill` bigint NOT NULL,
    `item` bigint NOT NULL,
    CONSTRAINT `PK_item_unskill` PRIMARY KEY (`skill`, `item`),
    CONSTRAINT `FK_item_unskill_item_template_item` FOREIGN KEY (`item`) REFERENCES `item_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_item_unskill_skill_skill` FOREIGN KEY (`skill`) REFERENCES `skill` (`id`) ON DELETE CASCADE
);
CREATE TABLE `monster_template_simple_inventory` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `chance` float NOT NULL,
    `itemtemplateid` bigint NOT NULL,
    `maxCount` int NOT NULL,
    `minCount` int NOT NULL,
    `monstertemplateid` bigint NOT NULL,
    CONSTRAINT `PK_monster_template_simple_inventory` PRIMARY KEY (`id`),
    CONSTRAINT `FK_monster_template_simple_inventory_item_template_itemtemplatei` FOREIGN KEY (`itemtemplateid`) REFERENCES `item_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_monster_template_simple_inventory_monster_template_monstertem` FOREIGN KEY (`monstertemplateid`) REFERENCES `monster_template` (`id`) ON DELETE CASCADE
);
CREATE TABLE `character_effect` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `active` bit NOT NULL DEFAULT true,
    `character` bigint NOT NULL,
    `currentcombatcount` int,
    `currenttimeduration` int,
    `duration` longtext,
    `effect` bigint NOT NULL,
    `reusable` bit NOT NULL DEFAULT false,
    CONSTRAINT `PK_character_effect` PRIMARY KEY (`id`),
    CONSTRAINT `FK_character_effect_effect_effect` FOREIGN KEY (`effect`) REFERENCES `effect` (`id`) ON DELETE NO ACTION
);
CREATE TABLE `character_history` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `action` varchar(255) NOT NULL,
    `character` bigint NOT NULL,
    `data` json,
    `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `effect` bigint,
    `gm` bit NOT NULL DEFAULT false,
    `info` longtext,
    `item` bigint,
    `modifier` bigint,
    `user` bigint,
    CONSTRAINT `PK_character_history` PRIMARY KEY (`id`),
    CONSTRAINT `FK_character_history_effect_effect` FOREIGN KEY (`effect`) REFERENCES `effect` (`id`) ON DELETE NO ACTION,
    CONSTRAINT `FK_character_history_user_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE SET NULL
);
CREATE TABLE `character_modifier` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `active` bit NOT NULL DEFAULT true,
    `character` bigint,
    `combatcount` int,
    `currentcombatcount` int,
    `currenttimeduration` int,
    `duration` longtext,
    `name` varchar(255) NOT NULL,
    `permanent` bit NOT NULL,
    `reusable` bit NOT NULL DEFAULT false,
    `timeduration` int,
    CONSTRAINT `PK_character_modifier` PRIMARY KEY (`id`)
);
CREATE TABLE `character_modifier_value` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `charactermodifier` bigint NOT NULL,
    `stat` varchar(255) NOT NULL,
    `type` varchar(255) NOT NULL DEFAULT 'ADD',
    `value` smallint NOT NULL,
    CONSTRAINT `PK_character_modifier_value` PRIMARY KEY (`id`),
    CONSTRAINT `FK_character_modifier_value_character_modifier_charactermodifier` FOREIGN KEY (`charactermodifier`) REFERENCES `character_modifier` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_character_modifier_value_stat_stat` FOREIGN KEY (`stat`) REFERENCES `stat` (`name`) ON DELETE CASCADE
);
CREATE TABLE `character_skills` (
    `character` bigint NOT NULL,
    `skill` bigint NOT NULL,
    CONSTRAINT `PK_character_skills` PRIMARY KEY (`character`, `skill`),
    CONSTRAINT `FK_character_skills_skill_skill` FOREIGN KEY (`skill`) REFERENCES `skill` (`id`) ON DELETE CASCADE
);
CREATE TABLE `character_speciality` (
    `character` bigint NOT NULL,
    `speciality` bigint NOT NULL,
    CONSTRAINT `PK_character_speciality` PRIMARY KEY (`character`, `speciality`),
    CONSTRAINT `FK_character_speciality_speciality_speciality` FOREIGN KEY (`speciality`) REFERENCES `speciality` (`id`) ON DELETE CASCADE
);
CREATE TABLE `group_invitations` (
    `group` bigint NOT NULL,
    `character` bigint NOT NULL,
    `fromgroup` bit NOT NULL,
    CONSTRAINT `PK_group_invitations` PRIMARY KEY (`group`, `character`)
);
CREATE TABLE `item` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `characterid` bigint,
    `container` bigint,
    `data` json,
    `itemtemplateid` bigint NOT NULL,
    `lootid` bigint,
    `modifiers` json,
    `monsterid` bigint,
    CONSTRAINT `PK_item` PRIMARY KEY (`id`),
    CONSTRAINT `FK_item_item_template_itemtemplateid` FOREIGN KEY (`itemtemplateid`) REFERENCES `item_template` (`id`) ON DELETE NO ACTION
);
CREATE TABLE `monster` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `data` json,
    `dead` timestamp,
    `group` bigint NOT NULL,
    `lootid` bigint,
    `name` varchar(255) NOT NULL,
    `targetcharacterid` bigint,
    `targetmonsterid` bigint,
    CONSTRAINT `PK_monster` PRIMARY KEY (`id`),
    CONSTRAINT `FK_monster_monster_targetmonsterid` FOREIGN KEY (`targetmonsterid`) REFERENCES `monster` (`id`) ON DELETE SET NULL
);
CREATE TABLE `character` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `active` bit NOT NULL DEFAULT true,
    `ad` smallint NOT NULL,
    `cha` smallint NOT NULL,
    `color` varchar(255) NOT NULL DEFAULT '22DD22',
    `cou` smallint NOT NULL,
    `ea` smallint,
    `ev` smallint,
    `experience` bigint NOT NULL,
    `fatepoint` smallint NOT NULL DEFAULT 0,
    `fo` smallint NOT NULL,
    `gmdata` json,
    `group` bigint,
    `int` smallint NOT NULL,
    `isnpc` bit NOT NULL DEFAULT false,
    `job` bigint,
    `level` smallint NOT NULL,
    `name` varchar(255) NOT NULL,
    `origin` bigint NOT NULL,
    `sexe` varchar(255) NOT NULL,
    `statbonusad` varchar(255),
    `targetcharacterid` bigint,
    `targetmonsterid` bigint,
    `user` bigint,
    CONSTRAINT `PK_character` PRIMARY KEY (`id`),
    CONSTRAINT `FK_character_job_job` FOREIGN KEY (`job`) REFERENCES `job` (`id`) ON DELETE NO ACTION,
    CONSTRAINT `FK_character_origin_origin` FOREIGN KEY (`origin`) REFERENCES `origin` (`id`) ON DELETE NO ACTION,
    CONSTRAINT `FK_character_character_targetcharacterid` FOREIGN KEY (`targetcharacterid`) REFERENCES `character` (`id`) ON DELETE SET NULL,
    CONSTRAINT `FK_character_monster_targetmonsterid` FOREIGN KEY (`targetmonsterid`) REFERENCES `monster` (`id`) ON DELETE SET NULL,
    CONSTRAINT `FK_character_user_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE
);
CREATE TABLE `group_history` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `action` varchar(255) NOT NULL,
    `data` json,
    `date` timestamp NOT NULL DEFAULT now(),
    `gm` bit NOT NULL,
    `group` bigint NOT NULL,
    `info` longtext,
    `user` bigint,
    CONSTRAINT `PK_group_history` PRIMARY KEY (`id`),
    CONSTRAINT `FK_group_history_user_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION
);
CREATE TABLE `loot` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `dead` datetime(6),
    `groupid` bigint NOT NULL,
    `name` varchar(255) NOT NULL,
    `visibleForPlayer` bit NOT NULL,
    CONSTRAINT `PK_loot` PRIMARY KEY (`id`)
);
CREATE TABLE `group` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `combatlootid` bigint NULL,
    `data` json,
    `location` bigint NOT NULL DEFAULT 1,
    `master` bigint,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_group` PRIMARY KEY (`id`),
    CONSTRAINT `FK_group_loot_combatlootid` FOREIGN KEY (`combatlootid`) REFERENCES `loot` (`id`) ON DELETE SET NULL,
    CONSTRAINT `FK_group_location_location` FOREIGN KEY (`location`) REFERENCES `location` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_group_user_master` FOREIGN KEY (`master`) REFERENCES `user` (`id`) ON DELETE NO ACTION
);
CREATE TABLE `quest` (
    `id` bigint AUTO_INCREMENT NOT NULL,
    `data` longtext NOT NULL,
    `group` bigint NOT NULL,
    `name` varchar(255) NOT NULL,
    CONSTRAINT `PK_quest` PRIMARY KEY (`id`),
    CONSTRAINT `FK_quest_group_group` FOREIGN KEY (`group`) REFERENCES `group` (`id`) ON DELETE CASCADE
);
CREATE INDEX `IX_character_group` ON `character` (`group`);
CREATE INDEX `IX_character_job` ON `character` (`job`);
CREATE INDEX `IX_character_origin` ON `character` (`origin`);
CREATE INDEX `IX_character_targetcharacterid` ON `character` (`targetcharacterid`);
CREATE INDEX `IX_character_targetmonsterid` ON `character` (`targetmonsterid`);
CREATE INDEX `IX_character_user` ON `character` (`user`);
CREATE INDEX `IX_character_effect_character` ON `character_effect` (`character`);
CREATE INDEX `IX_character_effect_effect` ON `character_effect` (`effect`);
CREATE INDEX `IX_character_history_character` ON `character_history` (`character`);
CREATE INDEX `IX_character_history_effect` ON `character_history` (`effect`);
CREATE INDEX `IX_character_history_item` ON `character_history` (`item`);
CREATE INDEX `IX_character_history_modifier` ON `character_history` (`modifier`);
CREATE INDEX `IX_character_history_user` ON `character_history` (`user`);
CREATE INDEX `IX_character_modifier_character` ON `character_modifier` (`character`);
CREATE INDEX `IX_character_modifier_value_charactermodifier` ON `character_modifier_value` (`charactermodifier`);
CREATE INDEX `IX_character_modifier_value_stat` ON `character_modifier_value` (`stat`);
CREATE INDEX `IX_character_skills_character` ON `character_skills` (`character`);
CREATE INDEX `IX_character_skills_skill` ON `character_skills` (`skill`);
CREATE INDEX `IX_character_speciality_speciality` ON `character_speciality` (`speciality`);
CREATE INDEX `IX_effect_category` ON `effect` (`category`);
CREATE INDEX `IX_effect_modifier_stat` ON `effect_modifier` (`stat`);
CREATE UNIQUE INDEX `IX_group_combatlootid` ON `group` (`combatlootid`);
CREATE INDEX `IX_group_location` ON `group` (`location`);
CREATE INDEX `IX_group_master` ON `group` (`master`);
CREATE INDEX `IX_group_history_group` ON `group_history` (`group`);
CREATE INDEX `IX_group_history_user` ON `group_history` (`user`);
CREATE INDEX `IX_group_invitations_character` ON `group_invitations` (`character`);
CREATE INDEX `IX_group_invitations_group` ON `group_invitations` (`group`);
CREATE INDEX `IX_item_characterid` ON `item` (`characterid`);
CREATE INDEX `IX_item_container` ON `item` (`container`);
CREATE INDEX `IX_item_itemtemplateid` ON `item` (`itemtemplateid`);
CREATE INDEX `IX_item_lootid` ON `item` (`lootid`);
CREATE INDEX `IX_item_monsterid` ON `item` (`monsterid`);
CREATE INDEX `IX_item_category_type` ON `item_category` (`type`);
CREATE INDEX `IX_item_effect_item` ON `item_effect` (`item`);
CREATE INDEX `IX_item_effect_requirejob` ON `item_effect` (`requirejob`);
CREATE INDEX `IX_item_effect_requireorigin` ON `item_effect` (`requireorigin`);
CREATE INDEX `IX_item_effect_stat` ON `item_effect` (`stat`);
CREATE INDEX `IX_item_requirement_item` ON `item_requirement` (`item`);
CREATE INDEX `IX_item_requirement_stat` ON `item_requirement` (`stat`);
CREATE INDEX `IX_item_skill_item` ON `item_skill` (`item`);
CREATE INDEX `IX_item_skill_skill` ON `item_skill` (`skill`);
CREATE INDEX `IX_item_skill_modifiers_item` ON `item_skill_modifiers` (`item`);
CREATE INDEX `IX_item_skill_modifiers_skill` ON `item_skill_modifiers` (`skill`);
CREATE INDEX `IX_item_template_category` ON `item_template` (`category`);
CREATE INDEX `IX_item_template_slot_item` ON `item_template_slot` (`item`);
CREATE INDEX `IX_item_unskill_item` ON `item_unskill` (`item`);
CREATE INDEX `IX_item_unskill_skill` ON `item_unskill` (`skill`);
CREATE INDEX `IX_job_parentjob` ON `job` (`parentjob`);
CREATE INDEX `IX_job_bonus_jobid` ON `job_bonus` (`jobid`);
CREATE INDEX `IX_job_origin_blacklist_job` ON `job_origin_blacklist` (`job`);
CREATE INDEX `IX_job_origin_blacklist_origin` ON `job_origin_blacklist` (`origin`);
CREATE INDEX `IX_job_origin_whitelist_job` ON `job_origin_whitelist` (`job`);
CREATE INDEX `IX_job_origin_whitelist_origin` ON `job_origin_whitelist` (`origin`);
CREATE INDEX `IX_job_requirement_jobid` ON `job_requirement` (`jobid`);
CREATE INDEX `IX_job_requirement_stat` ON `job_requirement` (`stat`);
CREATE INDEX `IX_job_restrict_jobid` ON `job_restrict` (`jobid`);
CREATE INDEX `IX_job_skill_skillid` ON `job_skill` (`skillid`);
CREATE INDEX `IX_location_parent` ON `location` (`parent`);
CREATE INDEX `IX_location_map_location` ON `location_map` (`location`);
CREATE INDEX `IX_loot_groupid` ON `loot` (`groupid`);
CREATE INDEX `IX_monster_group` ON `monster` (`group`);
CREATE INDEX `IX_monster_lootid` ON `monster` (`lootid`);
CREATE INDEX `IX_monster_targetcharacterid` ON `monster` (`targetcharacterid`);
CREATE INDEX `IX_monster_targetmonsterid` ON `monster` (`targetmonsterid`);
CREATE INDEX `IX_monster_location_locationid` ON `monster_location` (`locationid`);
CREATE INDEX `IX_monster_template_type` ON `monster_template` (`type`);
CREATE INDEX `IX_monster_template_simple_inventory_itemtemplateid` ON `monster_template_simple_inventory` (`itemtemplateid`);
CREATE INDEX `IX_monster_template_simple_inventory_monstertemplateid` ON `monster_template_simple_inventory` (`monstertemplateid`);
CREATE INDEX `IX_origin_bonus_originid` ON `origin_bonus` (`originid`);
CREATE INDEX `IX_origin_info_originid` ON `origin_info` (`originid`);
CREATE INDEX `IX_origin_requirement_originid` ON `origin_requirement` (`originid`);
CREATE INDEX `IX_origin_requirement_stat` ON `origin_requirement` (`stat`);
CREATE INDEX `IX_origin_restrict_originid` ON `origin_restrict` (`originid`);
CREATE INDEX `IX_origin_skill_skillid` ON `origin_skill` (`skillid`);
CREATE INDEX `IX_quest_group` ON `quest` (`group`);
CREATE INDEX `IX_skill_effect_skill` ON `skill_effect` (`skill`);
CREATE INDEX `IX_skill_effect_stat` ON `skill_effect` (`stat`);
CREATE INDEX `IX_speciality_job` ON `speciality` (`job`);
CREATE INDEX `IX_speciality_modifier_speciality` ON `speciality_modifier` (`speciality`);
CREATE INDEX `IX_speciality_modifier_stat` ON `speciality_modifier` (`stat`);
CREATE INDEX `IX_speciality_special_speciality` ON `speciality_special` (`speciality`);
CREATE INDEX `IX_spell_category` ON `spell` (`category`);
CREATE INDEX `IX_spell_teststat` ON `spell` (`teststat`);
CREATE UNIQUE INDEX `IX_user_username` ON `user` (`username`);
CREATE UNIQUE INDEX `IX_user_session_key` ON `user_session` (`key`);
CREATE INDEX `IX_user_session_userid` ON `user_session` (`userid`);
ALTER TABLE `character_effect` ADD CONSTRAINT `FK_character_effect_character_character` FOREIGN KEY (`character`) REFERENCES `character` (`id`) ON DELETE CASCADE;
ALTER TABLE `character_history` ADD CONSTRAINT `FK_character_history_character_character` FOREIGN KEY (`character`) REFERENCES `character` (`id`) ON DELETE CASCADE;
ALTER TABLE `character_history` ADD CONSTRAINT `FK_character_history_item_item` FOREIGN KEY (`item`) REFERENCES `item` (`id`) ON DELETE SET NULL;
ALTER TABLE `character_history` ADD CONSTRAINT `FK_character_history_character_modifier_modifier` FOREIGN KEY (`modifier`) REFERENCES `character_modifier` (`id`) ON DELETE RESTRICT;
ALTER TABLE `character_modifier` ADD CONSTRAINT `FK_character_modifier_character_character` FOREIGN KEY (`character`) REFERENCES `character` (`id`) ON DELETE SET NULL;
ALTER TABLE `character_skills` ADD CONSTRAINT `FK_character_skills_character_character` FOREIGN KEY (`character`) REFERENCES `character` (`id`) ON DELETE CASCADE;
ALTER TABLE `character_speciality` ADD CONSTRAINT `FK_character_speciality_character_character` FOREIGN KEY (`character`) REFERENCES `character` (`id`) ON DELETE CASCADE;
ALTER TABLE `group_invitations` ADD CONSTRAINT `FK_group_invitations_group_group` FOREIGN KEY (`group`) REFERENCES `group` (`id`) ON DELETE CASCADE;
ALTER TABLE `group_invitations` ADD CONSTRAINT `FK_group_invitations_character_character` FOREIGN KEY (`character`) REFERENCES `character` (`id`) ON DELETE CASCADE;
ALTER TABLE `item` ADD CONSTRAINT `FK_item_monster_monsterid` FOREIGN KEY (`monsterid`) REFERENCES `monster` (`id`) ON DELETE SET NULL;
ALTER TABLE `item` ADD CONSTRAINT `FK_item_character_characterid` FOREIGN KEY (`characterid`) REFERENCES `character` (`id`) ON DELETE CASCADE;
ALTER TABLE `item` ADD CONSTRAINT `FK_item_loot_lootid` FOREIGN KEY (`lootid`) REFERENCES `loot` (`id`) ON DELETE CASCADE;
ALTER TABLE `monster` ADD CONSTRAINT `FK_monster_group_group` FOREIGN KEY (`group`) REFERENCES `group` (`id`) ON DELETE CASCADE;
ALTER TABLE `monster` ADD CONSTRAINT `FK_monster_character_targetcharacterid` FOREIGN KEY (`targetcharacterid`) REFERENCES `character` (`id`) ON DELETE SET NULL;
ALTER TABLE `monster` ADD CONSTRAINT `FK_monster_loot_lootid` FOREIGN KEY (`lootid`) REFERENCES `loot` (`id`) ON DELETE SET NULL;
ALTER TABLE `character` ADD CONSTRAINT `FK_character_group_group` FOREIGN KEY (`group`) REFERENCES `group` (`id`) ON DELETE SET NULL;
ALTER TABLE `group_history` ADD CONSTRAINT `FK_group_history_group_group` FOREIGN KEY (`group`) REFERENCES `group` (`id`) ON DELETE CASCADE;
ALTER TABLE `loot` ADD CONSTRAINT `FK_loot_group_groupid` FOREIGN KEY (`groupid`) REFERENCES `group` (`id`) ON DELETE CASCADE;
