CREATE TABLE monster_category
(
 id BIGINT PRIMARY KEY AUTO_INCREMENT,
 name VARCHAR(255) NOT NULL,
 typeid BIGINT NOT NULL
);
ALTER TABLE monster_type DROP difficulty;

ALTER TABLE monster_category
ADD CONSTRAINT monster_category_monster_type_id_fk
FOREIGN KEY (typeid) REFERENCES monster_type (id);

INSERT INTO monster_category(id,name,typeid) (SELECT id,name,1 FROM monster_type);

ALTER TABLE `monster_template`
DROP FOREIGN KEY `FK_monster_template_monster_type_type`,
ADD FOREIGN KEY (`type`) REFERENCES `monster_category` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT

DELETE FROM `monster_type` WHERE `id` > 1;
UPDATE `monster_type` SET `name` = 'Non trié' WHERE `id` = 1;
ALTER TABLE `monster_template`
CHANGE `type` `categoryid` bigint(20) NOT NULL AFTER `name`;
