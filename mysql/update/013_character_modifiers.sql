ALTER TABLE character_modifier ADD type TEXT NULL;
ALTER TABLE character_modifier ADD description TEXT NULL;
ALTER TABLE effect_modifier MODIFY value SMALLINT NOT NULL;
