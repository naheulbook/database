ALTER TABLE character_effect ADD durationtype VARCHAR(255) NULL;
UPDATE character_effect SET durationtype='custom' WHERE `duration` IS NOT NULL;
UPDATE character_effect SET durationtype='combat' WHERE `currentcombatcount` IS NOT NULL;
UPDATE character_effect SET durationtype='lap' WHERE `currentlapcount` IS NOT NULL;
UPDATE character_effect SET durationtype='time' WHERE `currenttimeduration` IS NOT NULL;
UPDATE character_effect SET durationtype='forever' WHERE `durationtype` IS NULL;

 
