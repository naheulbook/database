CREATE TABLE god
(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `displayname` VARCHAR(255) NOT NULL,
    `description` TEXT,
    `techname` VARCHAR(255)
);
CREATE UNIQUE INDEX god_displayname_uindex ON `god` (displayname);
CREATE UNIQUE INDEX god_techname_uindex ON `god` (techname);