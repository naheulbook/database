ALTER TABLE character_modifier ADD durationtype VARCHAR(255) NULL;
UPDATE character_modifier SET durationtype='custom' WHERE `duration` IS NOT NULL;
UPDATE character_modifier SET durationtype='combat' WHERE `combatcount` IS NOT NULL;
UPDATE character_modifier SET durationtype='lap' WHERE `lapcount` IS NOT NULL;
UPDATE character_modifier SET durationtype='time' WHERE `timeduration` IS NOT NULL;
UPDATE character_modifier SET durationtype='forever' WHERE `durationtype` IS NULL AND NOT permanent;
 
