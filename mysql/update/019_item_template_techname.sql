ALTER TABLE item_template ADD techname VARCHAR(255) NULL;
CREATE UNIQUE INDEX item_template_techname_uindex ON naheulbook.item_template (techname);


UPDATE `item_template` SET `techname` = 'MONEY' WHERE `id` = 569;
UPDATE `item_template` SET `techname` = 'SMALL_PURSE' WHERE `id` = 606;
UPDATE `item_template` SET `techname` = 'MEDIUM_PURSE' WHERE `id` = 607;
UPDATE `item_template` SET `techname` = 'BIG_PURSE' WHERE `id` = 609;

