ALTER TABLE item_template ADD source ENUM('private', 'official', 'community') DEFAULT 'official' NULL;
ALTER TABLE item_template ADD sourceuserid BIGINT NULL;
ALTER TABLE item_template ADD CONSTRAINT item_template_user_id_fk
FOREIGN KEY (sourceuserid) REFERENCES user (id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE item_template ADD sourceusernamecache TEXT NULL;
