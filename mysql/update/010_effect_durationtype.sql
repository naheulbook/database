ALTER TABLE effect ADD durationtype VARCHAR(255) NULL;
UPDATE effect SET durationtype='custom' WHERE `duration` IS NOT NULL;
UPDATE effect SET durationtype='combat' WHERE `combatcount` IS NOT NULL;
UPDATE effect SET durationtype='lap' WHERE `lapcount` IS NOT NULL;
UPDATE effect SET durationtype='time' WHERE `timeduration` IS NOT NULL;
UPDATE effect SET durationtype='forever' WHERE `durationtype` IS NULL;

