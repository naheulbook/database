CREATE TABLE event
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  groupId BIGINT,
  name VARCHAR(255) NOT NULL,
  description TEXT,
  timestamp BIGINT NOT NULL,
  CONSTRAINT event_group_id_fk FOREIGN KEY (groupId) REFERENCES `group` (id)
);
ALTER TABLE event DROP FOREIGN KEY event_group_id_fk;
ALTER TABLE event
ADD CONSTRAINT event_group_id_fk
FOREIGN KEY (groupId) REFERENCES `group` (id) ON DELETE CASCADE ON UPDATE CASCADE;

