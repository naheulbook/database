ALTER TABLE `item_type` RENAME TO `item_template_section`;
ALTER TABLE `item_category` RENAME TO `item_template_category`;
ALTER TABLE `item_skill` RENAME TO `item_template_skill`;
ALTER TABLE `item_unskill` RENAME TO `item_template_unskill`;
ALTER TABLE `item_skill_modifiers` RENAME TO `item_template_skill_modifiers`;
ALTER TABLE `item_requirement` RENAME TO `item_template_requirement`;
ALTER TABLE `item_effect` RENAME TO `item_template_modifier`;

ALTER TABLE `item_template_category` CHANGE `type` `section` BIGINT(20) NOT NULL;
ALTER TABLE `item_template_slot` CHANGE `item` `itemTemplate` BIGINT(20) NOT NULL;
ALTER TABLE `item_template_skill` CHANGE `item` `itemTemplate` BIGINT(20) NOT NULL;
ALTER TABLE `item_template_unskill` CHANGE `item` `itemTemplate` BIGINT(20) NOT NULL;
ALTER TABLE `item_template_skill_modifiers` CHANGE `item` `itemTemplate` BIGINT(20) NOT NULL;
ALTER TABLE `item_template_requirement` CHANGE `item` `itemTemplate` BIGINT(20) NOT NULL;
ALTER TABLE `item_template_modifier` CHANGE `item` `itemTemplate` BIGINT(20) NOT NULL;

CREATE TABLE `item_type`
        (
        `id` INT PRIMARY KEY AUTO_INCREMENT,
        `techName` VARCHAR(255) NOT NULL,
        `displayName` VARCHAR(255) NOT NULL
        );

CREATE UNIQUE INDEX item_type_name_uindex ON `item_type` (techName);
CREATE UNIQUE INDEX item_type_displayName_uindex ON `item_type` (displayName);