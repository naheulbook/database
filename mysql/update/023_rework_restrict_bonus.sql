ALTER TABLE `origin_bonus` ADD `flags` JSON NULL;
ALTER TABLE `origin_restrict` ADD `flags` JSON NULL;
ALTER TABLE `job_bonus` ADD `flags` JSON NULL;
ALTER TABLE `job_restrict` ADD `flags` JSON NULL;
ALTER TABLE `speciality_special` ADD `flags` JSON NULL;
ALTER TABLE `origin` ADD `flags` JSON NULL;
ALTER TABLE `job` ADD `flags` JSON NULL;

UPDATE `job` SET `flags` = JSON_ARRAY(JSON_OBJECT('type', `special`)) WHERE `special` IS NOT NULL;
UPDATE `origin` SET `flags` = JSON_ARRAY(JSON_OBJECT('type', `special`)) WHERE `special` IS NOT NULL;
UPDATE `origin_bonus` SET `flags` = JSON_ARRAY(JSON_OBJECT('type', `token`)) WHERE `token` IS NOT NULL;
UPDATE `origin_restrict` SET `flags` = JSON_ARRAY(JSON_OBJECT('type', `tokens`)) WHERE `tokens` IS NOT NULL;
UPDATE `origin_restrict` SET `flags` = NULL WHERE `tokens` = '';
UPDATE `job_bonus` SET `flags` = JSON_ARRAY(JSON_OBJECT('type', `token`)) WHERE `token` IS NOT NULL;
UPDATE `speciality_special` SET `flags` = JSON_ARRAY(JSON_OBJECT('type', `token`)) WHERE `token` IS NOT NULL;

ALTER TABLE `origin` DROP `special`;
ALTER TABLE `job` DROP `special`;
ALTER TABLE `origin_bonus` DROP `token`;
ALTER TABLE `origin_restrict` DROP `tokens`;
ALTER TABLE `job_bonus` DROP `token`;
ALTER TABLE `speciality_special` DROP `token`;
