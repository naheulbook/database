CREATE TABLE effect_type
(
 id BIGINT,
 name VARCHAR(255)
 );
ALTER TABLE effect_type ADD PRIMARY KEY (id);

INSERT INTO `effect_type`(`id`,`name`) VALUES
(1, 'Mutation'),
(2, 'Critique'),
(3, 'Poison'),
(4, 'Magie'),
(5, 'Autre');

ALTER TABLE effect_category ADD typeid BIGINT NULL;

UPDATE `effect_category` SET `typeid` = 1 WHERE `id` = 1;
UPDATE `effect_category` SET `typeid` = 1 WHERE `id` = 2;
UPDATE `effect_category` SET `typeid` = 2 WHERE `id` = 3;
UPDATE `effect_category` SET `typeid` = 2 WHERE `id` = 4;
UPDATE `effect_category` SET `typeid` = 3 WHERE `id` = 5;
UPDATE `effect_category` SET `typeid` = 3 WHERE `id` = 6;
UPDATE `effect_category` SET `typeid` = 5 WHERE `id` = 7;
ALTER TABLE effect_category
  ADD CONSTRAINT effect_category_effect_type_id_fk
FOREIGN KEY (typeid) REFERENCES effect_type (id);

ALTER TABLE effect_category MODIFY note LONGTEXT;