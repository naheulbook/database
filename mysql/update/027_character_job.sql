CREATE TABLE character_job
(
 `character` BIGINT,
 `job` BIGINT,
 `order` INT,
 CONSTRAINT `character_job_character_id_fk` FOREIGN KEY (`character`) REFERENCES `character` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `character_job_job_id_fk` FOREIGN KEY (`job`) REFERENCES `job` (`id`) ON UPDATE CASCADE
 );

INSERT INTO `character_job` (`character`, `job`, `order`) SELECT `id`, `job`, 1 FROM `character`;
CREATE UNIQUE INDEX character_job_character_job_uindex ON `character_job` (`character`, `job`);

ALTER TABLE `character` DROP FOREIGN KEY FK_character_job_job;
DROP INDEX IX_character_job ON `character`;
ALTER TABLE `character` DROP job;
