ALTER TABLE `item` DROP `lifetimetype`;
ALTER TABLE `item` ADD `lifetimetype` VARCHAR(30) AS (JSON_UNQUOTE(data->"$.lifetime.durationType"));

